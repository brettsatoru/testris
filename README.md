#Testris' README
In order to get started with this project, you are going to need SFML 2.4.2 (32-bit edition).  2.4.1 and lower does not work for Visual Studio 2017, which is what this project was built against. This project is built with the assumption you saved the SFML SDK in the following location - "C:\Program Files (x86)\SFML\SFML-2.4.2\" so you'll want to save it in a similar location or make local adjustments as necessary.
The final goal of this project is to build a tetris clone using SFML, which will require sound and some pomp and circumstance.

#SFML
You can get SFML at [the official SFML website](https://www.sfml-dev.org/download/sfml/2.4.2/), be sure to grab the 32-bit version for C++ 14 for 2.4.2.  This program is compiled based on that.