@echo off
XCOPY /y "openal32.dll" %2
XCOPY /y "sfml-audio-2.dll" %2
XCOPY /y "sfml-graphics-2.dll" %2
XCOPY /y "sfml-network-2.dll" %2
XCOPY /y "sfml-system-2.dll" %2
XCOPY /y "sfml-window-2.dll" %2
XCOPY /y /s %1resource %2resource\