#include "Block.h"

#include <iostream>
#include <string>

Block::Block(sf::Color color, ersdk::GridWindow grid) : ersdk::GameObject(new sf::RectangleShape(grid.GetGridCellDimensions()))
{
	sf::RectangleShape *block = dynamic_cast<sf::RectangleShape *>(GetRenderTarget());
	mColor = sf::Color(color.r, color.g, color.b, 255);

	block->setOutlineThickness(1.f);
	block->setOutlineColor(sf::Color(0, 0, 0, 255));

	if (block != 0)
	{
		block->setFillColor(mColor);
	}
	mPosition = sf::Vector2u(0, 0);
	Visible(true);
}

void Block::SetNewDimensions(ersdk::GridWindow grid)
{
	dynamic_cast<sf::RectangleShape *>(GetRenderTarget())->setSize(grid.GetGridCellDimensions());
}

sf::Vector2u *Block::GetPosition()
{
	return &mPosition;
}

void Block::SetWorldPosition(ersdk::GridWindow grid)
{
	sf::RectangleShape *rect = dynamic_cast<sf::RectangleShape *>(GetRenderTarget());
	if (rect != 0)
	{
		rect->setFillColor(mColor);
		rect->setSize(grid.GetGridCellDimensions());
		rect->setPosition(grid.GetPositionByIndices(mPosition));
	}
	else
	{
		std::cerr << "Could not find the RectangleShape" << std::endl;
	}
}
