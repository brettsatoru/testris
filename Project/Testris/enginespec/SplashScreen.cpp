#include "SplashScreen.h"

ersdk::Scene *SplashScreenScene::RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int rngSeed)
{
	int wWidth = window->getSize().x;
	int wHeight = window->getSize().y;

	sf::Font calibri;
	sf::Text splashText;
	sf::Text mottoText;
	if (calibri.loadFromFile("resource\\fonts\\calibri.ttf"))
	{
		splashText.setFont(calibri);
		splashText.setCharacterSize(80);
		splashText.setString("ENLIGHT RESEARCH");

		sf::FloatRect splashBounds = splashText.getLocalBounds();
		splashText.setOrigin(sf::Vector2f(
			splashBounds.width / 2.f, splashBounds.height / 2.f));

		splashText.setPosition(sf::Vector2f(
			((float)wWidth / 2), ((float)wHeight / 2) - 20.f));

		splashText.setFillColor(sf::Color::Yellow);
		
		mottoText.setFont(calibri);
		mottoText.setString("\"Because we can't be wrong!\"");

		sf::FloatRect mottoBounds = mottoText.getLocalBounds();
		mottoText.setOrigin(sf::Vector2f(mottoBounds.width / 2.f, mottoBounds.height / 2.f));
		mottoText.setPosition(sf::Vector2f((float)wWidth / 2, splashText.getPosition().y + splashBounds.height + 10.f));
		mottoText.setFillColor(sf::Color::White);
	}

	clock->RegisterNewEvent("splashtime", 2000);

	bool skipOut = false;
	while (!clock->PeekEventComplete("splashtime")->TimeoutComplete())
	{
		sf::Event cEvent;
		while (window->pollEvent(cEvent))
		{
			if (cEvent.type == sf::Event::Closed)
			{
				window->close();
				skipOut = true;
			}
		}

		if (skipOut)
		{
			break;
		}

		clock->RollFrameTime();

		window->clear();
		window->draw(splashText);
		window->draw(mottoText);
		window->display();
	}

	clock->RemoveOldEvent("splashtime");

	return 0;
}