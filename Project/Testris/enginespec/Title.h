#ifndef TITLE_SCENE_H
#define TITLE_SCENE_H

#include <Scene.h>

class TitleScene : public ersdk::Scene
{
public:
	ersdk::Scene *RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int seed);
};

#endif // TITLE_SCENE_H