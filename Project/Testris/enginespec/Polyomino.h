#ifndef EXP_POLYOMINO
#define EXP_POLYOMINO

#include <SFML\Graphics.hpp>
#include "Block.h"
#include "BlockPile.h"
#include <vector>
#include <map>
#include <tuple>
#include <GridWindow.h>

#define RelCoord(X, Y) std::tuple<int, int>(X, Y)

enum PolyominoOrientation
{
	BASE,
	RIGHT,
	REVERSE,
	LEFT
};

/// Defines a Polymino, there is a control block and the rest of the block positions are based on that 'core' block.
class Polyomino
{
private:
	BlockPile *pBlockPile;  // Meant to be a reference, can be null representing no place to put a polyomino, indicating that the memory for it should be freed, not transferred.
	ersdk::GridWindow *pGridWindow; // Can be rebound, used to position polyomino in world space.
	sf::Vector2u mCorePosition;
	std::vector < sf::Vector2i > mRelativePositions;
	PolyominoOrientation mOrientation;
	std::map< std::tuple<int, int>, Block *> mBlocks;
	sf::Color mColor;

	sf::Vector2i GetOrientedPosition(sf::Vector2i baseOrientation, PolyominoOrientation cOrient);
	bool CheckNewOrientation(PolyominoOrientation nOrient);
public:
	Polyomino(sf::Vector2u cPosition, std::vector< sf::Vector2i > rPositions, sf::Color color, ersdk::GridWindow *tGrid, BlockPile *tPile = 0);
	~Polyomino();

	void SetWorldPosition();
	void BindNewGrid(ersdk::GridWindow *tGrid);

	bool ShiftRow(int x);
	bool MoveDown();
	bool IsSafe(); // Gets a value indicating if any of the polyomino blocks overlap something in the blockpile, indicating failure.

	bool ClockwiseTurn();
	bool CounterClockwiseTurn();

	std::vector< Block*> GetBlocks();
};

#define TPOLY { sf::Vector2i(-1, 0), sf::Vector2i(0, -1), sf::Vector2i(1, 0) }
#define TOSZPOLY_OFFSET sf::Vector2u(0, 1)
#define LPOLY { sf::Vector2i(-1, 0), sf::Vector2i(1, 0), sf::Vector2i(-1, 1) }
#define LJPOLY_OFFSET sf::Vector2u(0, 0)
#define JPOLY { sf::Vector2i(-1, 0), sf::Vector2i(1, 0), sf::Vector2i( 1, 1) }
#define IPOLY { sf::Vector2i(-2, 0), sf::Vector2i(-1, 0), sf::Vector2i(1, 0) }
#define IPOLY_OFFSET sf::Vector2u(0, 0)
#define OPOLY { sf::Vector2i(0, -1), sf::Vector2i(1, -1), sf::Vector2i(1, 0) }
#define SPOLY { sf::Vector2i(-1, 0), sf::Vector2i(0, -1), sf::Vector2i(1, -1)}
#define ZPOLY { sf::Vector2i(1, 0), sf::Vector2i(0, -1), sf::Vector2i(-1, -1)}

Polyomino *GetNewPolyomino(unsigned int selector, sf::Vector2u cPosition, ersdk::GridWindow *tGrid, BlockPile *tPile);

#endif
