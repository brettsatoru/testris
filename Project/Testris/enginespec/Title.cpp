#include "Title.h"

#include <SFML\Graphics.hpp>
#include "GameScene.h"

ersdk::Scene *TitleScene::RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int seed)
{
	// Initialize scene.
	int wWidth, wHeight;
	ersdk::Scene *result = 0; // initialize to nothing.

	if (window == 0)
	{
		return result;
	}

	if (!window->isOpen())
	{
		return result;
	}

	wWidth = window->getSize().x;
	wHeight = window->getSize().y;

	sf::Font calibri;
	sf::Text titleText;
	sf::Text startText;
	sf::Text exitText;
	if (calibri.loadFromFile("resource/fonts/calibri.ttf"))
	{
		titleText.setFillColor(sf::Color::Yellow);
		titleText.setFont(calibri);
		titleText.setCharacterSize(titleText.getCharacterSize() * 5);
		titleText.setString("TESTRIS");
		sf::FloatRect titleBounds = titleText.getLocalBounds();
		titleText.setOrigin(sf::Vector2f(titleBounds.width / 2, titleBounds.height / 2));
		titleText.setPosition(sf::Vector2f(wWidth / 2.f, wHeight / 3.f));

		startText.setFont(calibri);
		startText.setString("Press SPACE to begin.");
		sf::FloatRect startBounds = startText.getLocalBounds();
		startText.setOrigin(sf::Vector2f(startBounds.width / 2, startBounds.height / 2));
		startText.setPosition(sf::Vector2f(wWidth / 2.f, wHeight * (6.f / 8.f)));

		exitText.setFont(calibri);
		exitText.setString("Press ESC to quit.");
		sf::FloatRect exitBounds = exitText.getLocalBounds();
		exitText.setOrigin(sf::Vector2f(exitBounds.width / 2, exitBounds.height / 2));
		exitText.setPosition(sf::Vector2f(wWidth / 2.f, wHeight * (7.f / 8.f)));
	}

	bool readyToMove = false;
	while (!readyToMove)
	{
		sf::Event cEvent;

		while (window->pollEvent(cEvent))
		{
			if (cEvent.type == sf::Event::Closed)
			{
				result = 0;
				readyToMove = true;
			}

			if (cEvent.type == sf::Event::KeyPressed)
			{
				switch (cEvent.key.code)
				{
					case sf::Keyboard::Escape:
					{
						readyToMove = true;
					} break;
					case sf::Keyboard::Space:
					{
						result = new GameScene();
						readyToMove = true;
					} break;
				}
			}
		}

		window->clear();
		window->draw(titleText);
		window->draw(startText);
		window->draw(exitText);
		window->display();
	}

	return result;
}