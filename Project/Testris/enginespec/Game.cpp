#include "GameScene.h"
#include <random>
#include <GridWindow.h>
#include "BlockPile.h"
#include "Polyomino.h"

#include <iostream>
#include <string>

GameScene::GameScene()
{
	mGameState = RUN;
}

ersdk::Scene *GameScene::RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int rngSeed)
{
	int wWidth = window->getSize().x;
	int wHeight = window->getSize().y;
	unsigned int linesCleared = 0, score = 0;
	unsigned int threshold = 0;

	// Setup the mersenne twister
	std::mersenne_twister_engine<std::uint_fast32_t, 32, 624, 397, 31,
		0x9908B0DF, 11,
		0xFFFFFFFF, 7,
		0x9D2C5680, 15,
		0xEFC60000, 18, 1812433253> rng;
	rng.seed(rngSeed);

	ersdk::GridWindow mainGrid(sf::Vector2f(250.f, 500.f), sf::Vector2u(7, 14), sf::Vector2f(275.f, 50.f));
	ersdk::GridWindow previewGrid(sf::Vector2f((250.0f / 7) * 5, (250.0f / 7) * 3), sf::Vector2u(5, 3), sf::Vector2f(75.f, 75.f));

	BlockPile mainGridPile(&mainGrid);
	Polyomino *playeromino = GetNewPolyomino(rng(), sf::Vector2u(3, 0), &mainGrid, &mainGridPile);
	Polyomino *previewmino = GetNewPolyomino(rng(), sf::Vector2u(2, 0), &previewGrid, &mainGridPile);

	sf::RectangleShape gridRect(mainGrid.GetGridDimensions());
	gridRect.setOutlineThickness(2.f);
	gridRect.setOutlineColor(sf::Color::White);
	gridRect.setFillColor(sf::Color(64, 64, 64, 255));
	gridRect.setPosition(mainGrid.GetGridOffset());

	sf::RectangleShape previewRect(previewGrid.GetGridDimensions() + sf::Vector2f(0, 25.f));
	previewRect.setOutlineThickness(2.f);
	previewRect.setOutlineColor(sf::Color::White);
	previewRect.setFillColor(sf::Color::Black);
	previewRect.setPosition(previewGrid.GetGridOffset() - sf::Vector2f(0, 25.f));

	sf::RectangleShape gameOverRect(sf::Vector2f(0.f, 0.f));
	gameOverRect.setOutlineThickness(2.f);
	gameOverRect.setOutlineColor(sf::Color::White);
	gameOverRect.setFillColor(sf::Color::Black);

	previewmino->SetWorldPosition();

	// Set the game clock.
	clock->RegisterNewEvent("falldown", (unsigned int)floor(((1500.f / ((float)threshold + .75f)) + .3f)));

	sf::Font calibri;
	sf::Text scoreText;
	sf::Text linesText;
	sf::Text scoreLabelText;
	sf::Text linesLabelText;
	sf::Text gameOverText;
	sf::Text pressAnyKeyText;
	sf::Text goScoreLabelText;
	sf::Text goLinesLabelText;
	sf::Text goScoreText;
	sf::Text goLinesText;
	bool fontLoaded = false;
	if (calibri.loadFromFile("resource\\fonts\\calibri.ttf"))
	{
		scoreText.setFont(calibri);
		linesText.setFont(calibri);
		scoreLabelText.setFont(calibri);
		linesLabelText.setFont(calibri);
		gameOverText.setFont(calibri);
		pressAnyKeyText.setFont(calibri);
		goScoreLabelText.setFont(calibri);
		goLinesLabelText.setFont(calibri);
		goScoreText.setFont(calibri);
		goLinesText.setFont(calibri);
		fontLoaded = true;

		scoreLabelText.setString("SCORE");
		linesLabelText.setString("LINES");
		scoreText.setString(std::to_string(score));
		linesText.setString(std::to_string(linesCleared));

		float LabelXPosition = mainGrid.GetGridDimensions().x + mainGrid.GetGridOffset().x + 10.f;

		scoreLabelText.setPosition(sf::Vector2f(LabelXPosition, ((float)wHeight / 2.f) - 120.f));
		scoreText.setPosition(scoreLabelText.getPosition() + sf::Vector2f(0.f, 30.f));
		linesLabelText.setPosition(sf::Vector2f(LabelXPosition, ((float)wHeight / 2.f) - 30.f));
		linesText.setPosition(linesLabelText.getPosition() + sf::Vector2f(0.f, 30.f));

		// Manage the GO objects
		gameOverText.setCharacterSize((unsigned int)floor((float)gameOverText.getCharacterSize() * 1.5f));
		gameOverText.setString("Game Over");
		gameOverText.setOrigin(sf::Vector2f(gameOverText.getLocalBounds().width / 2, gameOverText.getLocalBounds().height / 2));
		gameOverText.setPosition(sf::Vector2f((float)wWidth / 2.f, (float)wHeight / 4.f));

		pressAnyKeyText.setString("Press Space...");
		pressAnyKeyText.setOrigin(sf::Vector2f(
			pressAnyKeyText.getLocalBounds().width / 2, pressAnyKeyText.getLocalBounds().height / 2));
		pressAnyKeyText.setPosition(gameOverText.getPosition() + sf::Vector2f(0.f, gameOverText.getLocalBounds().height * (7.f / 4.f)));

		goScoreLabelText.setString("Final Score: ");
		goScoreLabelText.setOrigin(sf::Vector2f(
			goScoreLabelText.getLocalBounds().width, goScoreLabelText.getLocalBounds().height / 2));
		goScoreLabelText.setPosition(pressAnyKeyText.getPosition() + sf::Vector2f(-5.f, pressAnyKeyText.getLocalBounds().height + 10.f));

		goLinesLabelText.setString("Final Lines: ");
		goLinesLabelText.setOrigin(sf::Vector2f(
			goLinesLabelText.getLocalBounds().width, goScoreLabelText.getLocalBounds().height / 2));
		goLinesLabelText.setPosition(goScoreLabelText.getPosition() + sf::Vector2f(0.f, goScoreLabelText.getLocalBounds().height + 10.f));

		goScoreText.setString(std::to_string(score));
		goScoreText.setOrigin(sf::Vector2f(0.f, goScoreText.getLocalBounds().height / 2));
		goScoreText.setPosition(goScoreLabelText.getPosition() + sf::Vector2f(10.f, 0.f));

		goLinesText.setString(std::to_string(linesCleared));
		goLinesText.setOrigin(sf::Vector2f(0., goLinesText.getLocalBounds().height / 2));
		goLinesText.setPosition(goLinesLabelText.getPosition() + sf::Vector2f(10.f, 0.f));

		gameOverRect.setSize(sf::Vector2f(400.f, 300.f));
		gameOverRect.setOrigin(sf::Vector2f(200.f, 150.f));
		gameOverRect.setPosition(sf::Vector2f((float)wWidth / 2, ((float)wHeight / 2) - 75.f));
		
	}

	bool qfall = false;
	bool fKeyReleased = true;

	bool gameComplete = false; // when the game completes, end scene.

	while (!gameComplete)
	{
		sf::Event cEvent;

		while (window->pollEvent(cEvent))
		{
			if (cEvent.type == sf::Event::Closed)
			{
				gameComplete = true;
				window->close();
			}

			if (mGameState == RUN)
			{
				if (cEvent.type == sf::Event::KeyReleased)
				{
					switch (cEvent.key.code)
					{
					case sf::Keyboard::Escape:
					{
						gameComplete = true;
					} break;
					case sf::Keyboard::W:
					case sf::Keyboard::Up:
					{
						playeromino->ClockwiseTurn();
					} break;
					case sf::Keyboard::A:
					case sf::Keyboard::Left:
					{
						playeromino->ShiftRow(-1);
					} break;
					case sf::Keyboard::D:
					case sf::Keyboard::Right:
					{
						playeromino->ShiftRow(1);
					} break;
					case sf::Keyboard::S:
					case sf::Keyboard::Down:
					{
						qfall = false;
						fKeyReleased = true;
					} break;
					default:
					{

					} break;
					}
				}

				if (cEvent.type == sf::Event::KeyPressed)
				{
					switch (cEvent.key.code)
					{
					case sf::Keyboard::S:
					{
						if (fKeyReleased)
						{
							qfall = true;
							fKeyReleased = false;
						}
					} break;
					}
				}
			}
			else if (mGameState == GAMEOVER)
			{
				if (cEvent.type == sf::Event::KeyPressed)
				{
					switch (cEvent.key.code)
					{
					case sf::Keyboard::Space:
					{
						gameComplete = true;
					} break;
					}
				}
			}
		}

		clock->RollFrameTime();

		if (qfall)
		{
			sf::Time tRemaining = clock->PeekEventTime("falldown");
			if (tRemaining.asMilliseconds() > 10)
			{
				clock->RollEventForward("falldown", sf::milliseconds(tRemaining.asMilliseconds() - (tRemaining.asMilliseconds() / 120)));
			}
			else
			{
				clock->RollEventForward("falldown", tRemaining);
			}
		}

		// This sets the stage for ClockEvent systems.
		if (clock->PeekEventComplete("falldown")->TimeoutComplete() && mGameState != GAMEOVER)
		{
			if (!playeromino->MoveDown())
			{
				free(playeromino);
				playeromino = 0;
				playeromino = previewmino;
				playeromino->BindNewGrid(&mainGrid);
				playeromino->ShiftRow(1);
				previewmino = 0;
				previewmino = GetNewPolyomino(rng(), sf::Vector2u(2, 0),
					&previewGrid, &mainGridPile);

				previewmino->SetWorldPosition();

				unsigned int clinesCleared = mainGridPile.CheckBlockPile();

				linesCleared += clinesCleared;
				score += clinesCleared * 100;
				if (clinesCleared >= 4)
				{
					score += 100;
				}

				scoreText.setString(std::to_string(score));
				linesText.setString(std::to_string(linesCleared));

				if (clinesCleared > 0 && linesCleared % 10 == 0)
				{
					threshold++;
				}

				qfall = false;
			}

			if (!playeromino->IsSafe())
			{
				mGameState = GAMEOVER;

				goLinesText.setString(std::to_string(linesCleared));
				goScoreText.setString(std::to_string(score));

				// Change all blocks to gray.
				auto pileBlocks = mainGridPile.GetBlocks();
				for (auto iter = pileBlocks.begin(); iter != pileBlocks.end(); ++iter)
				{
					sf::RectangleShape *tSprite = dynamic_cast<sf::RectangleShape *>((*iter)->GetRenderTarget());

					if (tSprite != 0)
					{
						tSprite->setFillColor(sf::Color(128, 128, 128));
					}
				}
			}

			clock->PeekEventComplete("falldown")->ResetTimer((unsigned int)floor(((1500.f / ((float)threshold + .75f)) + .3f)));
		}

		playeromino->SetWorldPosition();

		window->clear();
		window->draw(gridRect);
		window->draw(previewRect);
		window->draw(scoreLabelText);
		window->draw(scoreText);
		window->draw(linesLabelText);
		window->draw(linesText);

		for (size_t index = 0; index < mainGridPile.GetBlocks().size(); index++)
		{
			mainGridPile.GetBlocks()[index]->Draw(window);
		}

		auto blocks = playeromino->GetBlocks();
		for (auto iter = blocks.begin(); iter != blocks.end(); ++iter)
		{
			sf::RectangleShape *cRect = dynamic_cast<sf::RectangleShape *>((*iter)->GetRenderTarget());
			(*iter)->Draw(window);
		}

		auto prevBlocks = previewmino->GetBlocks();
		for (auto iter = prevBlocks.begin(); iter != prevBlocks.end(); ++iter)
		{
			(*iter)->Draw(window);
		}

		if (mGameState == GAMEOVER)
		{
			window->draw(gameOverRect);
			window->draw(gameOverText);
			window->draw(pressAnyKeyText);
			window->draw(goScoreLabelText);
			window->draw(goLinesLabelText);
			window->draw(goLinesText);
			window->draw(goScoreText);
		}

		window->display();
	}

	free(playeromino);
	free(previewmino);

	return 0;
}