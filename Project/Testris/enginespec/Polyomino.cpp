#include "Polyomino.h"
#include <iostream>

static sf::Color cPalette[] = { sf::Color::Yellow, sf::Color::Blue, sf::Color(255, 0, 128),
sf::Color::Red, sf::Color::Cyan, sf::Color::Green, sf::Color(255, 128, 0) };

Polyomino::Polyomino(sf::Vector2u cPosition, std::vector< sf::Vector2i > rPositions, sf::Color color, ersdk::GridWindow *tGrid, BlockPile *tPile)
{

	mCorePosition = cPosition;
	mRelativePositions = rPositions;
	pBlockPile = tPile;
	pGridWindow = tGrid;
	mColor = color;
	mOrientation = BASE;

	using namespace std;
	mBlocks = map< tuple<int, int>, Block *>();
	if (pGridWindow == 0)
	{
		// Cannot create a polyomino if we have no grid to set it against.
		// Should be handled once any grid is bound later.
		return;
	}

	mBlocks[RelCoord(0, 0)] = new Block(mColor, *tGrid);
	for (auto iter = mRelativePositions.begin(); iter != mRelativePositions.end(); ++iter)
	{
		Block *cBlock = new Block(mColor, *tGrid);
		if (cBlock->GetRenderTarget() != 0)
		{
			mBlocks[RelCoord(iter->x, iter->y)] = cBlock;
		}

	}
}

// If the Polyomino is destroy and still has Blocks, free them from memory.
// The Polyomino will pass its blocks to the pile under normal circumstances and then clear out the
// vector containing them.
Polyomino::~Polyomino()
{
	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		free(iter->second);
	}
	mBlocks.empty();
	pBlockPile = 0;
	pGridWindow = 0;
}

sf::Vector2i Polyomino::GetOrientedPosition(sf::Vector2i base, PolyominoOrientation cOrient)
{
	switch (cOrient)
	{
	case LEFT:
	{
		int yValue = -base.y;
		base.y = base.x;
		base.x = yValue;
		return base;
	} break;
	case RIGHT:
	{
		int yValue = base.y;
		base.y = -base.x;
		base.x = yValue;
		return base;
	} break;
	case REVERSE:
	{
		return -base;
	} break;
	case BASE:
	default:
	{
		return base;
	} break;
	}
}

bool Polyomino::CheckNewOrientation(PolyominoOrientation nOrient)
{
	bool result = true;
	if (pGridWindow != 0)
	{
		for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
		{
			sf::Vector2i scPosition(mCorePosition);
			sf::Vector2i relPosition(std::get<0>(iter->first), std::get<1>(iter->first));
			scPosition += GetOrientedPosition(relPosition, nOrient);

			if (scPosition.x < 0 || ((unsigned int)abs(scPosition.x)) >= pGridWindow->GetGridSizeX())
			{
				result = false;
				break;
			}

			if (scPosition.y < 0 || ((unsigned int)abs(scPosition.y)) >= pGridWindow->GetGridSizeY())
			{
				result = false;
				break;
			}

			if (result && pBlockPile != 0)
			{
				sf::Vector2u cPosition(scPosition);
				result = !(pBlockPile->IsBlockAtLocation(cPosition));
				if (!result)
				{
					break;
				}
			}
		}
	}
	else
	{
		result = false; // Won't allow a reorientation when no grid is assigned.
	}

	return result;
}

/// Sets the world position of the blocks.
void Polyomino::SetWorldPosition()
{
	if (pGridWindow != 0)
	{
		for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
		{
			*(iter->second->GetPosition()) = mCorePosition + sf::Vector2u(GetOrientedPosition(sf::Vector2i(std::get<0>(iter->first), std::get<1>(iter->first)), mOrientation));
			iter->second->SetWorldPosition(*pGridWindow);
		}
	}
}

void Polyomino::BindNewGrid(ersdk::GridWindow *tGrid)
{
	if (pGridWindow == 0)
	{
		// Create the Blocks
		mBlocks[RelCoord(0, 0)] = new Block(mColor, *tGrid);
		for (auto iter = mRelativePositions.begin(); iter != mRelativePositions.end(); ++iter)
		{
			mBlocks[RelCoord(iter->x, iter->y)] = new Block(mColor, *tGrid);
		}
	}
	else
	{
		// Reassign the grid dimensions
		mBlocks[RelCoord(0, 0)]->SetNewDimensions(*tGrid);
		for (auto iter = mRelativePositions.begin(); iter != mRelativePositions.end(); ++iter)
		{
			mBlocks[RelCoord(iter->x, iter->y)]->SetNewDimensions(*tGrid);
		}
	}

	pGridWindow = tGrid;
}

bool Polyomino::ShiftRow(int x)
{
	bool result = true;
	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		sf::Vector2i scPosition(mCorePosition);
		sf::Vector2i relPosition(std::get<0>(iter->first), std::get<1>(iter->first));
		scPosition += GetOrientedPosition(relPosition, mOrientation);

		if (scPosition.x + x < 0 || ((unsigned int)(abs(scPosition.x) + x)) >= pGridWindow->GetGridSizeX())
		{
			result = false;
			break;
		}

		if (result && pBlockPile != 0)
		{
			sf::Vector2u cPosition(scPosition + sf::Vector2i(x, 0));
			result = !(pBlockPile->IsBlockAtLocation(cPosition));
			if (!result)
			{
				break;
			}
		}
	}

	if (result)
	{
		mCorePosition.x += x;
	}

	return result;
}

bool Polyomino::MoveDown()
{
	bool result = true;
	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		sf::Vector2i scPosition(mCorePosition);
		sf::Vector2i relPosition(std::get<0>(iter->first), std::get<1>(iter->first));
		scPosition += GetOrientedPosition(relPosition, mOrientation);

		if ((unsigned int)(abs(scPosition.y) + 1) >= pGridWindow->GetGridSizeY())
		{
			result = false;
			break;
		}
		else if (pBlockPile != 0)
		{
			if (pBlockPile->IsBlockAtLocation(sf::Vector2u(scPosition) + sf::Vector2u(0, 1)))
			{
				result = false;
				break;
			}
		}
	}

	if (result)
	{
		mCorePosition.y += 1;
	}
	else
	{
		// Pass all the blocks into the Pile.
		if (pBlockPile != 0)
		{
			for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
			{
				sf::Vector2i rPos = GetOrientedPosition(sf::Vector2i(std::get<0>(iter->first), std::get<1>(iter->first)), mOrientation);
				*(iter->second->GetPosition()) = sf::Vector2u(mCorePosition.x + rPos.x, mCorePosition.y + rPos.y);
				iter->second->SetWorldPosition(*pGridWindow);
				pBlockPile->AddBlock(iter->second);
			}

			mBlocks.empty();
			mRelativePositions.empty();
		}
	}

	return result;

}

bool Polyomino::IsSafe()
{
	// Get the blocks in the polyomino
	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		if (pBlockPile->GetBlockAtLocation(*(iter->second->GetPosition())))
		{
			return false;
		}
	}

	return true;
}

std::vector<Block*> Polyomino::GetBlocks()
{
	std::vector<Block *> result = std::vector<Block *>();

	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		dynamic_cast<sf::RectangleShape *>(iter->second->GetRenderTarget())->setFillColor(mColor);

		result.push_back(iter->second);
	}

	if (result.size() != 4)
	{
		std::cout << "We got " << std::to_string(result.size()) << " blocks" << std::endl;
	}
	return result;
}

bool Polyomino::ClockwiseTurn()
{
	PolyominoOrientation nOrient;
	switch (mOrientation)
	{
	case RIGHT:
	{
		nOrient = PolyominoOrientation::REVERSE;
	} break;
	case REVERSE:
	{
		nOrient = PolyominoOrientation::LEFT;
	} break;
	case LEFT:
	{
		nOrient = PolyominoOrientation::BASE;
	} break;
	case BASE:
	default:
	{
		nOrient = PolyominoOrientation::RIGHT;
	} break;
	}

	if (CheckNewOrientation(nOrient))
	{
		mOrientation = nOrient;
	}

	return false;
}

bool Polyomino::CounterClockwiseTurn()
{
	PolyominoOrientation nOrient;
	switch (mOrientation)
	{
	case RIGHT:
	{
		nOrient = PolyominoOrientation::BASE;
	} break;
	case REVERSE:
	{
		nOrient = PolyominoOrientation::RIGHT;
	} break;
	case LEFT:
	{
		nOrient = PolyominoOrientation::REVERSE;
	} break;
	case BASE:
	default:
	{
		nOrient = PolyominoOrientation::LEFT;
	} break;
	}

	if (CheckNewOrientation(nOrient))
	{
		mOrientation = nOrient;
	}

	return false;
}

Polyomino *GetNewPolyomino(unsigned int selector, sf::Vector2u cPosition, ersdk::GridWindow *tGrid, BlockPile *tPile)
{
	selector = selector % 7;
	sf::Color color = cPalette[selector];
	Polyomino *polymino;

	if (selector == 0u)
	{
		polymino = new Polyomino(cPosition + TOSZPOLY_OFFSET, TPOLY, color, tGrid, tPile);
	}
	else if (selector == 1u)
	{
		polymino = new Polyomino(cPosition + LJPOLY_OFFSET, LPOLY, color, tGrid, tPile);
	}
	else if (selector == 2u)
	{
		polymino = new Polyomino(cPosition + LJPOLY_OFFSET, JPOLY, color, tGrid, tPile);
	}
	else if (selector == 3u)
	{
		polymino = new Polyomino(cPosition + IPOLY_OFFSET, IPOLY, color, tGrid, tPile);
	}
	else if (selector == 4u)
	{
		polymino = new Polyomino(cPosition + TOSZPOLY_OFFSET, OPOLY, color, tGrid, tPile);
	}
	else if (selector == 5u)
	{
		polymino = new Polyomino(cPosition + TOSZPOLY_OFFSET, SPOLY, color, tGrid, tPile);
	}
	else if (selector == 6u)
	{
		polymino = new Polyomino(cPosition + TOSZPOLY_OFFSET, ZPOLY, color, tGrid, tPile);
	}


	return polymino;
}