#ifndef EXP_BLOCKPILE
#define EXP_BLOCKPILE

#include <SFML\Graphics.hpp>
#include "Block.h"
#include <GridWindow.h>
#include <map>
#include <vector>
#include <tuple>

#define BLOCK_KEY(X, Y) std::tuple<unsigned int, unsigned int>(X, Y)

class BlockPile
{
private:
	ersdk::GridWindow *pGrid; // A pointer in case anything in the grid we point to changes.
	std::map< std::tuple<unsigned int, unsigned int> , Block*> mBlocks; // Pointers to blocks, when we remove them they need to be freed from memory.

	void DropFromRow(unsigned int row);
	void ClearRow(unsigned int row);
public:
	BlockPile(ersdk::GridWindow *grid = 0);

	bool IsBlockAtLocation(sf::Vector2u coords);
	void AddBlock(Block *block);
	void AddBlocks(std::vector<Block *> blocks);

	std::vector<Block *> GetBlocks(); // For Drawing
	Block *GetBlockAtLocation(sf::Vector2u coords); // For doing things.
	unsigned int CheckBlockPile(); // Returns a value indicating lines cleared.
};

#endif