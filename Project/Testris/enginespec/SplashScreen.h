#ifndef EXP_SPLASH_SCREEN
#define EXP_SPLASH_SCREEN

#include <Scene.h>

class SplashScreenScene : public ersdk::Scene
{
public:
	ersdk::Scene *RunScene(sf::RenderWindow *window, ersdk::GameClock * clock, unsigned int rngSeed);
};

#endif
