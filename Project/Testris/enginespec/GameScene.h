#ifndef EXP_GAME_SCENE
#define EXP_GAME_SCENE

#include <Scene.h>

enum MainGameState {
	START, RUN, GAMEOVER
};

class GameScene : public ersdk::Scene
{
private:
	MainGameState mGameState;
public:
	GameScene();
	ersdk::Scene *RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int rngSeed);
};

#endif
