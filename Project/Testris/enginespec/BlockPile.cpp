#include "BlockPile.h"

BlockPile::BlockPile(ersdk::GridWindow *grid)
{
	pGrid = grid; // Set the grid.
	mBlocks = std::map< std::tuple<unsigned int, unsigned int> , Block*>();
}

bool BlockPile::IsBlockAtLocation(sf::Vector2u coords)
{
	std::tuple<unsigned int, unsigned int> tCoords(coords.x, coords.y);
	return mBlocks.count(tCoords) != 0;
}

void BlockPile::AddBlock(Block *block)
{
	sf::Vector2u coords = *(block->GetPosition());
	std::tuple<unsigned int, unsigned int> tCoords(coords.x, coords.y);
	mBlocks[tCoords] = block;
}

void BlockPile::AddBlocks(std::vector<Block *> blocks)
{
	for (size_t index = 0; index < blocks.size(); index++)
	{
		AddBlock(blocks[index]);
	}
}

std::vector<Block *> BlockPile::GetBlocks()
{
	std::vector<Block *> result = std::vector<Block *>();

	for (auto iter = mBlocks.begin(); iter != mBlocks.end(); ++iter)
	{
		result.push_back(iter->second);
	}

	return result;
}

Block *BlockPile::GetBlockAtLocation(sf::Vector2u coords)
{
	std::tuple<unsigned int, unsigned int> tCoords(coords.x, coords.y);
	if (mBlocks.count(tCoords))
	{
		return mBlocks[tCoords];
	}

	return 0;
}

unsigned int BlockPile::CheckBlockPile()
{
	std::vector<size_t> rowsCleared = std::vector<size_t>();
	for (size_t row = pGrid->GetGridSizeY(); row > 0; --row)
	{
		bool rowFull = true;
		for (size_t column = 0; column < pGrid->GetGridSizeX(); column++)
		{
			if (!IsBlockAtLocation(sf::Vector2u(column, row)))
			{
				rowFull = false;
				break;
			}
		}

		if (rowFull)
		{
			ClearRow(row);
			rowsCleared.push_back(row);
		}
	}

	for (auto iter = rowsCleared.end(); iter != rowsCleared.begin(); --iter)
	{
		size_t tRow = std::distance(rowsCleared.begin(), iter);
		DropFromRow(rowsCleared[tRow-1]);
	}

	return rowsCleared.size();
}

void BlockPile::DropFromRow(unsigned int row)
{
	for (int tRow = row; tRow >= 0; --tRow)
	{
		size_t cRow = tRow + 1;
		if (cRow >= pGrid->GetGridSizeY())
		{
			continue;
		}

		for (size_t col = 0; col < pGrid->GetGridSizeX(); col++)
		{
			auto key = BLOCK_KEY(col, tRow);
			auto ckey = BLOCK_KEY(col, cRow);
			if (mBlocks.count(key))
			{
				Block *tBlock = mBlocks[key];
				tBlock->GetPosition()->y = cRow;
				tBlock->SetWorldPosition(*pGrid);
				AddBlock(tBlock);
				mBlocks.erase(key);
			}
		}

	}
}

void BlockPile::ClearRow(unsigned int row)
{
	for (size_t col = 0; col < pGrid->GetGridSizeX(); col++)
	{
		auto key = BLOCK_KEY(col, row);
		if (mBlocks.count(key))
		{
			// Free the memory of the block
			free(mBlocks[key]->GetRenderTarget());
			free(mBlocks[key]);
			mBlocks.erase(key);
		}
	}
}