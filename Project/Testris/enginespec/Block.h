#ifndef EXP_BLOCK
#define EXP_BLOCK

#include <SFML\Graphics.hpp>
#include <GridWindow.h>
#include <GameObject.h>

/// Defines a single block
class Block : public ersdk::GameObject
{
private:
	sf::Vector2u mPosition;
	sf::Color mColor;
public:
	Block(sf::Color color, ersdk::GridWindow grid);

	void SetNewDimensions(ersdk::GridWindow grid);
	sf::Vector2u *GetPosition();

	void SetWorldPosition(ersdk::GridWindow grid);
};

#endif