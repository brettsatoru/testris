#ifndef ERSDK_GAMECLOCK
#define ERSDK_GAMECLOCK

#include <SFML\Graphics.hpp>
#include <string>
#include <vector>
#include <map>
#include <ClockEvent.h>

namespace ersdk
{
	enum EventInsertStatus
	{
		EVENT_OK = 0,
		EVENT_EXISTS = 1,
		EVENT_UNMAKEABLE = 2
	};

	/// A clock responsible for handling GameTime.
	class GameClock
	{
	private:
		sf::Clock mClock;
		sf::Time mTimeSinceStart;
		sf::Time mElapsedTimeLastFrame;
		sf::Time mDeltaTime;
		std::vector< sf::Time > mSceneTime;
		std::map< std::string, ClockEvent > mStopWatches;
	public:
		GameClock();

		sf::Time GetDeltaTime();
		sf::Time TimeSinceGameStart();
		int RegisterNewSceneTime();
		void ReleaseSceneTime(int index);
		sf::Time GetTimeSinceSceneStart(int index);

		void RollFrameTime();

		EventInsertStatus RegisterNewEvent(std::string name, unsigned int timeToDie);
		void RemoveOldEvent(std::string name);
		ClockEvent *PeekEventComplete(std::string name);
		void RollEventForward(std::string name, sf::Time forwardTime);
		sf::Time PeekEventTime(std::string name);
	};
}

#endif