#ifndef ERSDK_CLOCKEVENT
#define ERSDK_CLOCKEVENT

#include <SFML\Graphics.hpp>

namespace ersdk
{
	class ClockEvent
	{
	private:
		sf::Time mTimeToDie;
		sf::Time mElapsedTime;
	public:
		ClockEvent(unsigned int timeToDie = 0);

		bool TimeoutComplete();
		void RollTimer(sf::Time dt);
		void ResetTimer(unsigned int newTTD = 0);
		bool InvalidEvent();
		sf::Time GetTimeRemaining();
	};
}

#define EMPTY_CLOCK_EVENT ersdk::ClockEvent();

#endif
