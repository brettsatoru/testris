#ifndef ERSDK_GAMEOBJECT
#define ERSDK_GAMEOBJECT

#include <SFML\Graphics.hpp>

namespace ersdk
{
	/// Responsible for holding onto and operating on game logic and components.
	class GameObject
	{
	private:
		sf::Drawable *pDrawable;
		bool mVisible;
	public:
		GameObject(sf::Drawable *target = 0);
		~GameObject();

		sf::Drawable *GetRenderTarget();
		sf::Drawable *SetRenderTarget(sf::Drawable *target);

		void Draw(sf::RenderWindow *wHandle);

		bool IsVisible();
		void Visible(bool value);

		// Update Logic
		// Collision Logic

	};
}
#endif
