#include <GameObject.h>

using namespace ersdk;

GameObject::GameObject(sf::Drawable *target)
{
	pDrawable = target;
}

GameObject::~GameObject()
{
	if (pDrawable == 0)
	{
		free(pDrawable);
		pDrawable = 0;
	}
}

sf::Drawable *GameObject::GetRenderTarget()
{
	return pDrawable;
}

sf::Drawable *GameObject::SetRenderTarget(sf::Drawable *target)
{
	sf::Drawable *oldTarget = pDrawable;
	pDrawable = target;
	return oldTarget;
}

void GameObject::Draw(sf::RenderWindow *wHandle)
{
	if (IsVisible())
	{
		wHandle->draw(*pDrawable);
	}
}

bool GameObject::IsVisible()
{
	return mVisible;
}

void GameObject::Visible(bool value)
{
	mVisible = value;
}