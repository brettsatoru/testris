#include <GridWindow.h>

using namespace ersdk;

GridWindow::GridWindow()
{
	mGridDimensions = sf::Vector2f(0.f, 0.f);
	mGridIndices = sf::Vector2u(0, 0);
}

GridWindow::GridWindow(sf::Vector2f dimensions, sf::Vector2u indices, sf::Vector2f offset)
{
	mGridDimensions = dimensions;
	mGridIndices = indices;
	mGlobalOffset = offset;
}

GridWindow::GridWindow(sf::RenderWindow *window, sf::Vector2u indices)
{
	sf::Vector2u wSize = window->getSize();
	mGridDimensions = sf::Vector2f((float)wSize.x, (float)wSize.y);
	mGridIndices = indices;
}

#pragma region GridDimensions

sf::Vector2f GridWindow::GetGridDimensions()
{
	return mGridDimensions;
}

void GridWindow::SetGridDimensions(float width, float height)
{
	mGridDimensions.x = (width < 0.f) ? 0.f : width;
	mGridDimensions.y = (height < 0.f) ? 0.f : height;
}

void GridWindow::SetGridDimensions(sf::Vector2f dimensions)
{
	SetGridDimensions(dimensions.x, dimensions.y);
}

#pragma endregion

#pragma region GridIndices

sf::Vector2u GridWindow::GetGridIndices()
{
	return mGridIndices;
}

void GridWindow::SetGridIndices(int width, int height)
{
	mGridIndices.x = (width < 0) ? 0 : width;
	mGridIndices.y = (height < 0) ? 0 : height;
}

void GridWindow::SetGridIndices(sf::Vector2u indices)
{
	SetGridIndices(indices.x, indices.y);
}

#pragma endregion

sf::Vector2f GridWindow::GetGridCellDimensions()
{
	sf::Vector2f result(0.f, 0.f);

	if (mGridIndices.x != 0)
	{
		result.x = mGridDimensions.x / (float)mGridIndices.x;
	}

	if (mGridIndices.y != 0)
	{
		result.y = mGridDimensions.y / (float)mGridIndices.y;
	}

	return result;
}

sf::Vector2f GridWindow::GetPositionByIndices(uint32 width, uint32 height)
{
	sf::Vector2f cellSize = GetGridCellDimensions();
	sf::Vector2f result(0.f, 0.f);

	width = (mGridIndices.x == 0) ? 0 : width % mGridIndices.x; // Wrap the width.
	height = (mGridIndices.y == 0) ? 0 : height % mGridIndices.y; // Wrap the height.

	if (width > 0)
	{
		result.x = cellSize.x * width;
	}

	if (height > 0)
	{
		result.y = cellSize.y * height;
	}

	return result + mGlobalOffset;
}

sf::Vector2f GridWindow::GetPositionByIndices(sf::Vector2u indices)
{
	return GetPositionByIndices(indices.x, indices.y);
}

sf::Vector2f GridWindow::GetGridOffset()
{
	return mGlobalOffset;
}

void GridWindow::SetGridOffset(float width, float height)
{
	mGlobalOffset = sf::Vector2f(width, height);
}

void GridWindow::SetGridOffset(sf::Vector2f offset)
{
	mGlobalOffset = offset;
}

size_t GridWindow::GetGridSizeX()
{
	return mGridIndices.x;
}

size_t GridWindow::GetGridSizeY()
{
	return mGridIndices.y;
}