#include <GameClock.h>

using namespace ersdk;

GameClock::GameClock()
{
	mSceneTime = std::vector<sf::Time>();
	mStopWatches = std::map<std::string, ClockEvent>();
	mClock = sf::Clock();
	mElapsedTimeLastFrame = sf::milliseconds(0);
}

sf::Time GameClock::GetDeltaTime()
{
	return mDeltaTime;
}

sf::Time GameClock::TimeSinceGameStart()
{
	return mClock.getElapsedTime();
}

int GameClock::RegisterNewSceneTime()
{
	mSceneTime.push_back(mClock.getElapsedTime());

	return mSceneTime.size() - 1;
}

void GameClock::ReleaseSceneTime(int index)
{
	size_t uindex = (unsigned int)abs(index);
	if (uindex > 0u && uindex < mSceneTime.size())
	{
		std::vector<sf::Time>::iterator stTarget = mSceneTime.begin() + index;
		mSceneTime.erase(stTarget);
	}
}

sf::Time GameClock::GetTimeSinceSceneStart(int index)
{
	size_t uindex = (unsigned int)abs(index);
	if (uindex > 0u && uindex < mSceneTime.size())
	{
		return mClock.getElapsedTime() - mSceneTime[index];
	}

	return sf::milliseconds(0); // Return no time elapsed as there is no valid scene
}

void GameClock::RollFrameTime()
{
	mDeltaTime = sf::milliseconds(mClock.getElapsedTime().asMilliseconds() - mElapsedTimeLastFrame.asMilliseconds());

	mElapsedTimeLastFrame = mClock.getElapsedTime();

	for (std::map<std::string, ClockEvent>::iterator it = mStopWatches.begin(); it != mStopWatches.end(); ++it)
	{
		it->second.RollTimer(mDeltaTime);
	}
}

EventInsertStatus GameClock::RegisterNewEvent(std::string name, unsigned int timeToDie)
{
	if (mStopWatches.count(name)) return EVENT_EXISTS;

	mStopWatches[name] = ClockEvent(timeToDie);

	if (mStopWatches.count(name)) return EVENT_UNMAKEABLE;

	return EVENT_OK;
}

void GameClock::RemoveOldEvent(std::string name)
{
	if (mStopWatches.count(name))
	{
		mStopWatches.erase(name);
	}
}

ClockEvent *GameClock::PeekEventComplete(std::string name)
{
	if (mStopWatches.count(name))
	{
		return &mStopWatches[name];
	}

	return new EMPTY_CLOCK_EVENT;
}

void GameClock::RollEventForward(std::string name, sf::Time forward)
{
	if (mStopWatches.count(name))
	{
		mStopWatches[name].RollTimer(forward);
	}
}

sf::Time GameClock::PeekEventTime(std::string name)
{
	if (mStopWatches.count(name))
	{
		return mStopWatches[name].GetTimeRemaining();
	}

	return sf::milliseconds(-1);
}