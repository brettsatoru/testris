#include <ClockEvent.h>

using namespace ersdk;

ClockEvent::ClockEvent(unsigned int timeToDie)
{
	mTimeToDie = sf::milliseconds(timeToDie);

	ResetTimer();
}

bool ClockEvent::TimeoutComplete()
{
	if (mTimeToDie == sf::Time::Zero) return false;

	return mElapsedTime.asMicroseconds() < 0;
}

void ClockEvent::RollTimer(sf::Time dt)
{
	if (!TimeoutComplete())
	{
		mElapsedTime = mElapsedTime - dt;
	}
}

void ClockEvent::ResetTimer(unsigned int newTTD)
{
	if (newTTD != 0)
	{
		mTimeToDie = sf::milliseconds(newTTD);
	}

	mElapsedTime = mTimeToDie;
}

bool ClockEvent::InvalidEvent()
{
	return !(mTimeToDie > sf::Time::Zero);
}

sf::Time ClockEvent::GetTimeRemaining()
{
	return mElapsedTime;
}