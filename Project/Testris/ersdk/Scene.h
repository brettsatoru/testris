#ifndef ERSDK_SCENE
#define ERSDK_SCENE

#include <SFML\Graphics.hpp>
#include <GameClock.h>

namespace ersdk
{
	class Scene
	{
	public:
		virtual ersdk::Scene *RunScene(sf::RenderWindow *window, ersdk::GameClock *clock, unsigned int rngSeed) = 0;
	};
}

#endif