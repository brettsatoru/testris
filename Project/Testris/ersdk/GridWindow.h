#ifndef ERSDK_GRIDWINDOW
#define ERSDK_GRIDWINDOW

#include <SFML\Graphics.hpp>

namespace ersdk
{
#define uint32 unsigned int

	// Responsible for holding and defining a grid.
	class GridWindow
	{
	private:
		sf::Vector2f mGlobalOffset;
		sf::Vector2f mGridDimensions;
		sf::Vector2u mGridIndices;

	public:
		// A blank grid window, for Zero As Initialization
		GridWindow();

		// For GridWindows that don't take up the whole screen
		GridWindow(sf::Vector2f dimensions, sf::Vector2u indices, sf::Vector2f offset);
		
		// For GridWindows that do take up the whole screen
		GridWindow(sf::RenderWindow *window, sf::Vector2u indices);

		sf::Vector2f GetGridDimensions();
		void SetGridDimensions(float width, float height);
		void SetGridDimensions(sf::Vector2f);

		sf::Vector2u GetGridIndices();
		void SetGridIndices(int width, int height);
		void SetGridIndices(sf::Vector2u);

		sf::Vector2f GetGridOffset();
		void SetGridOffset(float width, float height);
		void SetGridOffset(sf::Vector2f);

		sf::Vector2f GetGridCellDimensions();
		sf::Vector2f GetPositionByIndices(uint32 width, uint32 height);
		sf::Vector2f GetPositionByIndices(sf::Vector2u);

		size_t GetGridSizeX();
		size_t GetGridSizeY();
	};
}

#endif