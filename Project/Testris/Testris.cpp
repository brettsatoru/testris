// Testris.cpp : Defines the entry point for the console application.
//

#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>
#include <Windows.h>
#include <GameObject.h>
#include <GridWindow.h>
#include <GameClock.h>

#include "enginespec\SplashScreen.h"
#include "enginespec\GameScene.h"
#include "enginespec\Title.h"

#include <iostream>
#include <chrono>
#include <random>
#include <limits>

unsigned int GetSystemTimeInMilliseconds();

int main()
{
#ifdef NDEBUG
	ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif

	int wWidth, wHeight;

	wWidth = 800;
	wHeight = 600;

	sf::RenderWindow main(sf::VideoMode(wWidth, wHeight), "Block Dump");
	ersdk::GameClock globalClock;

	while (main.isOpen())
	{
		ersdk::Scene *splashScreen = new SplashScreenScene();

		globalClock.RollFrameTime();
		splashScreen->RunScene(&main, &globalClock, 0);

		free(splashScreen);
		splashScreen = 0;

		if (!main.isOpen())
		{
			break;
		}

		ersdk::Scene *cScene = new TitleScene();

		globalClock.RollFrameTime();

		while (cScene != 0)
		{
			ersdk::Scene *nScene = cScene->RunScene(&main, &globalClock, GetSystemTimeInMilliseconds());

			if (nScene != 0)
			{
				globalClock.RollFrameTime();
				nScene->RunScene(&main, &globalClock, GetSystemTimeInMilliseconds());
				free(nScene);
			}
			else
			{
				free(cScene);
				cScene = 0;
			}
		}
		
		main.close();
	}

    return 0;
}

unsigned int GetSystemTimeInMilliseconds()
{
	using namespace std::chrono;

	unsigned int ms = (duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count()) % std::numeric_limits<unsigned int>::max();

	return ms;
}
